//returning html elements functions
function getElement(selector) {
  return document.querySelector(selector);
}

function getElements(selector) {
  return document.querySelectorAll(selector);
}

//menu animations
let menuBtn = document.querySelector(".menu-btn");
let nav = document.querySelector("nav");

menuBtn.addEventListener("click", function() {
  nav.classList.toggle("nav-opened");
  this.classList.toggle("close");
});

//gallery tabs animations
let galleryCategories = getElements("ul.gallery-categories li a");
let galleryPages = getElements(".gallery-grid");

//page tracker
let current = 0;

//media query for lightbox
var mediaQuery = window.matchMedia("(max-width: 768px)");
function watchMedia(mediaQuery, lightboxTarget) {
  if (mediaQuery.matches) {
    return;
  } else {
    lightbox(lightboxTarget);
  }
}

watchMedia(mediaQuery, current);
mediaQuery.addListener(watchMedia);

galleryCategories.forEach((galleryCategory, index) => {
  galleryCategory.addEventListener("click", function() {
    changeActiveCategory(this);
    nextPage(index);
    console.log(index);

    // lightbox with mediaquery
    watchMedia(mediaQuery, index);
    mediaQuery.addListener(watchMedia);
  });
});

//changing the activness the buttons
function changeActiveCategory(category) {
  galleryCategories.forEach(galleryCategory => {
    galleryCategory.classList.remove("active");
  });
  category.classList.add("active");
}

function nextPage(pageNumber) {
  const nextPage = galleryPages[pageNumber];
  const currentPage = galleryPages[current];

  currentPage.classList.remove("active-grid");
  nextPage.classList.add("active-grid");

  const tl = new TimelineMax({
    //doesnt allows to click next button unless current animation ends
    onStart: function() {
      galleryCategories.forEach(galleryCategory => {
        galleryCategory.style.pointerEvents = "none";
      });
    },

    onComplete: function() {
      galleryCategories.forEach(galleryCategory => {
        galleryCategory.className === "active" //condition
          ? (galleryCategory.style.pointerEvents = "none") // --> if condition is true
          : (galleryCategory.style.pointerEvents = "all"); // --> if condition is false (else)
      });
    }
  });
  //the animation part
  tl.fromTo(
    currentPage,
    0.7,
    { opacity: 1, x: "0%", display: "grid" },
    { opacity: 0, x: "120%", display: "none" }
  ).fromTo(
    nextPage,
    0.7,
    { opacity: 0, x: "-120%", display: "none" },
    { opacity: 1, x: "0%", display: "grid" }
  );

  //update the current page number
  current = pageNumber;
}

//lightbox
let modal = getElement(".gallery-modal");
let closeModalBtn = getElement(".close-modal");
let modalPrev = getElement("#modalPrev");
let modalNext = getElement("#modalNext");
let modalImageContainer = getElement(".gallery-modal .modal-img-container");
let modalImage = document.createElement("img");
modalImageContainer.appendChild(modalImage);

function lightbox(currentTab) {
  let lightboxImages = getElements(`.grid-index${currentTab} img`);

  function openLightbox() {
    const tlLightbox = new TimelineMax();

    tlLightbox.fromTo(modal, 0.3, { y: "-120%" }, { y: "0%" });

    closeModalBtn.addEventListener("click", () => {
      tlLightbox.reverse();
    });
  }

  function currentImage(targetImage) {
    modalImage.src = targetImage.src;
    checkOrient(targetImage);
  }

  function nextImage(thisImage) {
    modalImage.src = lightboxImages[thisImage + 1].src;
    checkOrient(modalImage);
  }

  function prevImage(thisImage) {
    modalImage.src = lightboxImages[thisImage - 1].src;
    checkOrient(modalImage);
  }

  //lightbox orientation
  function checkOrient(thisImage) {
    if (thisImage.clientHeight > thisImage.clientWidth) {
      modalImageContainer.classList.remove("modal-img-container-landscape");
    } else if (thisImage.clientHeight < thisImage.clientWidth) {
      modalImageContainer.classList.add("modal-img-container-landscape");
    }
  }

  lightboxImages.forEach((lightboxImage, imageIndex) => {
    let activeImage = lightboxImages[imageIndex];

    lightboxImage.addEventListener("click", function() {
      currentImage(activeImage);
      openLightbox();

      modalNext.addEventListener("click", function() {
        if (imageIndex >= lightboxImages.length - 1) {
          imageIndex = -1;
        }
        nextImage(imageIndex);
        imageIndex++;
      });

      modalPrev.addEventListener("click", function() {
        if (imageIndex <= 0) {
          imageIndex = lightboxImages.length;
        }
        prevImage(imageIndex);
        imageIndex--;
      });
    });
  });
}
